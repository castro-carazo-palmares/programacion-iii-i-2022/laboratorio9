<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ["isbn", "titulo", "numeroPaginas", "editorial", "idioma", "idGenero", "idAutor"];

    protected $primaryKey = "isbn";

    public function genres()
    {
        return $this->belongsTo("App\Models\Genre", "idGenero");
    }

    public function authors()
    {
        return $this->belongsTo("App\Models\Author", "idAutor");
    }

}
