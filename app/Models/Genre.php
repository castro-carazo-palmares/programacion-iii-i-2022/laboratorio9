<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    protected $fillable = ["nombre"];

    public function books()
    {
        return $this->hasMany("App\Models\Book", "idGenero");
    }
}
