<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Models\Genre;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('books.index', ['books'=>Book::with(['genres', 'authors'])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create', ['genres'=>Genre::get(), 'authors'=>Author::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookRequest $request)
    {
        $fields = $request->validate([
            'isbn' => 'required|numeric',
            'titulo' => 'required|string',
            'numeroPaginas' => 'required|numeric',
            'editorial' => 'required|string',
            'idioma' => 'required|string',
            'idGenero' => 'required|numeric',
            'idAutor' => 'required|numeric',
        ]);

        Book::create($fields);

        return redirect()->route('books.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('books.show', ['book' => $book]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('books.edit', ['book'=>$book, 'genres'=>Genre::get(), 'authors'=>Author::get()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookRequest  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request, Book $book)
    {
        $fields = $request->validate([
            'isbn' => 'required|numeric',
            'titulo' => 'required|string',
            'numeroPaginas' => 'required|numeric',
            'editorial' => 'required|string',
            'idioma' => 'required|string',
            'idGenero' => 'required|numeric',
            'idAutor' => 'required|numeric',
        ]);

        $book->update($fields);
        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index');
    }
}
