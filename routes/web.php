<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::resource("genres", "App\Http\Controllers\GenreController");
Route::resource("authors", "App\Http\Controllers\AuthorController");
Route::resource("books", "App\Http\Controllers\BookController");
