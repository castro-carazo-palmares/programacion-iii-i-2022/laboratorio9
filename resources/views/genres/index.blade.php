@extends('layout')

@section('title', 'Géneros')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Géneros</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton me-2" href="{{ route('welcome') }}">Regresar</a>
            <a class="boton" href="{{ route('genres.create') }}">Agregar Género</a>
        </div>
        <div class="row">
            @if(!$genres->isEmpty())
                <div class="container tabla">
                    <div class="row encabezado">
                        <div class="col-12 col-md-3 columna">ID</div>
                        <div class="col-12 col-md-3 columna">Nombre</div>
                        <div class="col-12 col-md-6 columna">Acciones</div>
                    </div>
                    @foreach($genres as $genre)
                    <div class="row contenido">
                        <div class="col-12 col-md-3 columna">{{ $genre->id }}</div>
                        <div class="col-12 col-md-3 columna">{{ $genre->nombre }}</div>
                        <div class="col-12 col-md-6 acciones columna">
                            <a class="boton mostrar col" href="{{ route('genres.show', $genre) }}">Mostrar</a>
                            <a class="boton editar col" href="{{ route('genres.edit', $genre) }}">Editar</a>
                            <form class="col" action="{{ route('genres.destroy', $genre) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="boton eliminar" type="submit" value="Eliminar">
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            @else
                <div class="col">
                    No existen géneros para mostrar
                </div>
            @endif
        </div>
    </div>
@endsection
