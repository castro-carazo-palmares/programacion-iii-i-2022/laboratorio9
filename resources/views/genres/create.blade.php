@extends('layout')

@section('title', 'Agregar Género')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Insertar Género</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('genres.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('genres.store') }}" method="POST">
                    @csrf

                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" required>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
