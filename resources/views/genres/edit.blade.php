@extends('layout')

@section('title', 'Modificar Género')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Modificar Género</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('genres.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('genres.update', $genre) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <label for="id">ID</label>
                    <input type="number" name="id" value="{{ $genre->id }}" readonly>

                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" value="{{ $genre->nombre }}" required>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
