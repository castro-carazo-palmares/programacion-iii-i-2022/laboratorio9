@extends('layout')

@section('title', 'Mostrar Género')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>{{ $genre->nombre }}</h1>
        </div>
        <div class="row acciones">
            <a class="boton" href="{{ route('genres.index') }}">Regresar</a>
        </div>
    </div>
@endsection
