@extends('layout')

@section('title', 'Agregar Autor')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Insertar Autor</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('authors.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('authors.store') }}" method="POST">
                    @csrf

                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" required>

                    <label for="apellidos">Apellidos</label>
                    <input type="text" name="apellidos" required>

                    <label for="nacionalidad">Nacionalidad</label>
                    <input type="text" name="nacionalidad" required>

                    <label for="fechaNacimiento">Fecha de nacimiento</label>
                    <input type="date" name="fechaNacimiento" required>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
