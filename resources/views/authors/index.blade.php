@extends('layout')

@section('title', 'Autores')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Autores</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton me-2" href="{{ route('welcome') }}">Regresar</a>
            <a class="boton" href="{{ route('authors.create') }}">Agregar Autor</a>
        </div>
        <div class="row">
            @if(!$authors->isEmpty())
                <div class="container tabla">
                    <div class="row encabezado">
                        <div class="col-12 col-md-3 columna">ID</div>
                        <div class="col-12 col-md-3 columna">Nombre</div>
                        <div class="col-12 col-md-6 columna">Acciones</div>
                    </div>
                    @foreach($authors as $author)
                    <div class="row contenido">
                        <div class="col-12 col-md-3 columna">{{ $author->id }}</div>
                        <div class="col-12 col-md-3 columna">{{ $author->nombre }}</div>
                        <div class="col-12 col-md-6 acciones columna">
                            <a class="boton mostrar col" href="{{ route('authors.show', $author) }}">Mostrar</a>
                            <a class="boton editar col" href="{{ route('authors.edit', $author) }}">Editar</a>
                            <form class="col" action="{{ route('authors.destroy', $author) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="boton eliminar" type="submit" value="Eliminar">
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            @else
                <div class="col">
                    No existen autores para mostrar
                </div>
            @endif
        </div>
    </div>
@endsection
