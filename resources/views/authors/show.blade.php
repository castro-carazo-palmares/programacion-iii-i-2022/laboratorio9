@extends('layout')

@section('title', 'Mostrar Autor')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>{{ $author->nombre }}</h1>
        </div>
        <div class="row acciones">
            <a class="boton" href="{{ route('authors.index') }}">Regresar</a>
        </div>
        <div class="row contenido">
            <h2>Nombre: {{ $author->nombre }}</h2>
            <h2>Apellidos: {{ $author->apellidos }}</h2>
            <h2>Nacionalidad: {{ $author->nacionalidad }}</h2>
            <h2>Fecha de nacimiento: {{ $author->fechaNacimiento }}</h2>
        </div>
    </div>
@endsection
