@extends('layout')

@section('title', 'Modificar Autor')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Modificar Autor</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('authors.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('authors.update', $author) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <label for="id">ID</label>
                    <input type="number" name="id" value="{{ $author->id }}" readonly>

                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" value="{{ $author->nombre }}" required>

                    <label for="apellidos">Apellidos</label>
                    <input type="text" name="apellidos" value="{{ $author->apellidos }}" required>

                    <label for="nacionalidad">Nacionalidad</label>
                    <input type="text" name="nacionalidad" value="{{ $author->nacionalidad }}" required>

                    <label for="fechaNacimiento">Fecha de nacimiento</label>
                    <input type="date" name="fechaNacimiento" value="{{ $author->fechaNacimiento }}" required>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
