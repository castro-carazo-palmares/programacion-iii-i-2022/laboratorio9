@extends('layout')

@section('title', 'Mostrar Libro')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>{{ $book->isbn }}</h1>
        </div>
        <div class="row acciones">
            <a class="boton" href="{{ route('books.index') }}">Regresar</a>
        </div>
        <div class="row contenido">
            <h2>ISBN: {{ $book->isbn }}</h2>
            <h2>Título: {{ $book->titulo }}</h2>
            <h2>Número de páginas: {{ $book->numeroPaginas }}</h2>
            <h2>Editorial: {{ $book->editorial }}</h2>
            <h2>Idioma: {{ $book->idioma }}</h2>
            <h2>Género: {{ $book->genres->nombre }}</h2>
            <h2>Autor: {{ $book->authors->nombre . " " . $book->authors->apellidos }}</h2>
        </div>
    </div>
@endsection
