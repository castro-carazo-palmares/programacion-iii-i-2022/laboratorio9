@extends('layout')

@section('title', 'Agregar Libro')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Insertar Libro</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('books.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('books.store') }}" method="POST">
                    @csrf

                    <label for="isbn">ISBN</label>
                    <input type="number" name="isbn" required>

                    <label for="titulo">Título</label>
                    <input type="text" name="titulo" required>

                    <label for="numeroPaginas">Número de páginas</label>
                    <input type="number" name="numeroPaginas" required>

                    <label for="editorial">Editorial</label>
                    <input type="text" name="editorial" required>

                    <label for="idioma">Idioma</label>
                    <input type="text" name="idioma" required>

                    <label for="idGenero">Género</label>
                    <select name="idGenero" id="idGenero" required>
                        @foreach($genres as $genre)
                            <option value="{{ $genre->id }}">{{ $genre->nombre }}</option>
                        @endforeach
                    </select>

                    <label for="idAutor">Autores</label>
                    <select name="idAutor" id="idAutor" required>
                        @foreach($authors as $author)
                            <option value="{{ $author->id }}">{{ $author->nombre }}</option>
                        @endforeach
                    </select>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
