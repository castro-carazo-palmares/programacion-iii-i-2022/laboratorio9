@extends('layout')

@section('title', 'Libros')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Libros</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton me-2" href="{{ route('welcome') }}">Regresar</a>
            <a class="boton" href="{{ route('books.create') }}">Agregar Libro</a>
        </div>
        <div class="row">
            @if(!$books->isEmpty())
                <div class="container tabla">
                    <div class="row encabezado">
                        <div class="col-12 col-md-3 columna">ISBN</div>
                        <div class="col-12 col-md-3 columna">Título</div>
                        <div class="col-12 col-md-6 columna">Acciones</div>
                    </div>
                    @foreach($books as $book)
                    <div class="row contenido">
                        <div class="col-12 col-md-3 columna">{{ $book->isbn }}</div>
                        <div class="col-12 col-md-3 columna">{{ $book->titulo }}</div>
                        <div class="col-12 col-md-6 acciones columna">
                            <a class="boton mostrar col" href="{{ route('books.show', $book) }}">Mostrar</a>
                            <a class="boton editar col" href="{{ route('books.edit', $book) }}">Editar</a>
                            <form class="col" action="{{ route('books.destroy', $book) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="boton eliminar" type="submit" value="Eliminar">
                            </form>
                        </div>
                    </div>
                    @endforeach
                </div>
            @else
                <div class="col">
                    No existen libros para mostrar
                </div>
            @endif
        </div>
    </div>
@endsection
