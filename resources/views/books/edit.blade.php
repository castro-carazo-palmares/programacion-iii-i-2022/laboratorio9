@extends('layout')

@section('title', 'Modificar Libro')

@section('content')
    <div class="container">
        <div class="row titulo">
            <h1>Modificar Libro</h1>
        </div>
        <div class="row acciones mb-4">
            <a class="boton" href="{{ route('books.index') }}">Regresar</a>
        </div>
        <div class="row formulario">
            <fieldset>
                <form action="{{ route('books.update', $book) }}" method="POST">
                    @csrf
                    @method('PATCH')

                    <label for="isbn">ISBN</label>
                    <input type="number" name="isbn" value="{{ $book->isbn }}" required readonly>

                    <label for="titulo">Título</label>
                    <input type="text" name="titulo" value="{{ $book->titulo }}" required>

                    <label for="numeroPaginas">Número de páginas</label>
                    <input type="number" name="numeroPaginas" value="{{ $book->numeroPaginas }}" required>

                    <label for="editorial">Editorial</label>
                    <input type="text" name="editorial" value="{{ $book->editorial }}" required>

                    <label for="idioma">Idioma</label>
                    <input type="text" name="idioma" value="{{ $book->idioma }}" required>

                    <label for="idGenero">Género</label>
                    <select name="idGenero" id="idGenero" required>
                        @foreach($genres as $genre)
                            <option value="{{ $genre->id }}" {{ $genre->id === $book->idGenero ? 'selected' : '' }}>{{ $genre->nombre }}</option>
                        @endforeach
                    </select>

                    <label for="idAutor">Autores</label>
                    <select name="idAutor" id="idAutor" required>
                        @foreach($authors as $author)
                            <option value="{{ $author->id }}" {{ $author->id === $book->idAutor ? 'selected' : '' }}>{{ $author->nombre }}</option>
                        @endforeach
                    </select>

                    <div class="acciones">
                        <input class="boton" type="submit" value="Guardar">
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
@endsection
